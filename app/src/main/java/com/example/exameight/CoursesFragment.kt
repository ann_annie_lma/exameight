package com.example.exameight

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.exameight.adapters.CurrentCoursesAdapter
import com.example.exameight.adapters.NewCoursesAdapter
import com.example.exameight.databinding.FragmentCoursesBinding
import com.example.exameight.network.Resource
import com.example.exameight.viewmodel.CoursesViewModel


class CoursesFragment : Fragment() {

    private val coursesViewModel:CoursesViewModel by viewModels()
    lateinit var newCoursesAdapter: NewCoursesAdapter
    lateinit var activeCourseAdapter : CurrentCoursesAdapter


    private var _binding: FragmentCoursesBinding? = null
    val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentCoursesBinding.inflate(inflater,container,false)
        return _binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        start()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    private fun start()
    {
        setUpRecycler()
        observers()
        coursesViewModel.load()
    }

    private fun setUpRecycler()
    {
        binding.rvNewCourses.apply {
            newCoursesAdapter = NewCoursesAdapter()
            adapter = newCoursesAdapter
            layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)

        }

        binding.rvExistingCourses.apply {
            activeCourseAdapter = CurrentCoursesAdapter()
            adapter = activeCourseAdapter
            layoutManager = LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false)
        }
    }

    private fun observers()
    {

        coursesViewModel.response.observe(viewLifecycleOwner, {response ->
            when(response)
            {
                is Resource.Success ->
                {
                    Toast.makeText(context,"Success",Toast.LENGTH_SHORT).show()
                    newCoursesAdapter.resultNewCourses = response.data!!.new_courses
                    activeCourseAdapter.resultActiveCourses = response.data!!.active_courses
                    //binding.tvCourses.text = response.data?.active_courses.toString()

                }
                is Resource.Failure ->
                {
                    Toast.makeText(context,"Error", Toast.LENGTH_SHORT).show()
                }

                is Resource.Loading ->
                {

                }
            }
        })

    }


}