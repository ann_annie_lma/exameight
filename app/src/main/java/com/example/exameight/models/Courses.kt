package com.example.exameight.models

data class Courses(
    val active_courses: List<ActiveCourse>,
    val new_courses: List<NewCourse>
)