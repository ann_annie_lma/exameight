package com.example.exameight.models

data class NewCourse(
    val duration: String,
    val icon_type: String,
    val id: String,
    val main_color: String,
    val question: String,
    val title: String
)