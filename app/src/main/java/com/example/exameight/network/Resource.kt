package com.example.exameight.network


sealed class Resource<T>(
        val data:T? = null,
        val errorMessage:String? = null
    )
    {
        class Success<T>(data:T?) : Resource<T> (data)
        class Failure<T>(errorMessage: String) : Resource<T>(errorMessage = errorMessage)
        class Loading<T> (loading:Boolean = true) : Resource<T>()
    }