package com.example.exameight.network

import com.example.exameight.models.Courses
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET(NetworkConstants.REPROSITORY_URL)
    suspend fun getData(): Response<Courses>

}