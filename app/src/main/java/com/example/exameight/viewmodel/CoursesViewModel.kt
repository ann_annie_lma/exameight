package com.example.exameight.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.exameight.models.Courses
import com.example.exameight.network.NetworkClient
import com.example.exameight.network.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CoursesViewModel : ViewModel() {



    private var _response: MutableLiveData<Resource<Courses>> = MutableLiveData()
    val response : LiveData<Resource<Courses>> get() = _response

    fun load()
    {
        viewModelScope.launch {
            withContext(Dispatchers.IO)
            {
                getCourses()
            }
        }
    }


    private suspend fun getCourses()
    {

        try {
            val result = NetworkClient.api.getData()
            val resultBody = result.body()
            if(result.code() == 200)
            {
                _response.postValue(Resource.Success(resultBody))
            }
            else
            {
                _response.postValue(Resource.Failure(result.message()))
            }

        } catch (e:Exception)
        {
            _response.postValue(Resource.Failure(e.message!!))
        }
    }


}