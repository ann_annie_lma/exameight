package com.example.exameight.adapters

import android.graphics.Color
import android.graphics.PorterDuff
import android.icu.util.TimeUnit
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.exameight.R
import com.example.exameight.databinding.NewCoursesItemBinding
import com.example.exameight.models.Courses
import com.example.exameight.models.NewCourse
import kotlin.system.measureTimeMillis


class NewCoursesAdapter : RecyclerView.Adapter<NewCoursesAdapter.NewCoursesViewHolder>()  {


    inner class NewCoursesViewHolder(val binding: NewCoursesItemBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun bind()
        {
            binding.apply {
                val newCourses = resultNewCourses[adapterPosition]
                tvTitle.text = newCourses.title
                tvWhat.text = newCourses.question
                root.setCardBackgroundColor(Color.parseColor("#"+newCourses.main_color))

                if(newCourses.icon_type == "card")
                {
                    Glide.with(ivIconType.getContext())  // glide with this view
                        .load(R.drawable.card)
                        .placeholder(R.mipmap.ic_launcher) // default placeHolder image
                        .error(R.mipmap.ic_launcher)
                        .centerCrop()// use this image if faile
                        .into(ivIconType)
                }else
                {
                    Glide.with(ivIconType.getContext())  // glide with this view
                        .load(R.drawable.settings)
                        .placeholder(R.mipmap.ic_launcher) // default placeHolder image
                        .error(R.mipmap.ic_launcher)
                        .centerCrop()// use this image if faile
                        .into(ivIconType)
                }

                tvDuration.text = millisToMinuts(1200000).toString() + " min"

                ivStart.setColorFilter(Color.parseColor("#"+newCourses.main_color), PorterDuff.Mode.SRC_IN);

            }

        }



        private fun millisToMinuts(millis:Long):Long
        {
            val mins = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(millis)
            return mins
        }

    }


    private val diffCallBack = object: DiffUtil.ItemCallback<NewCourse>()
    {
        override fun areItemsTheSame(oldItem: NewCourse, newItem: NewCourse): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: NewCourse, newItem: NewCourse): Boolean {
            return oldItem == newItem
        }

    }

    private val differ = AsyncListDiffer(this,diffCallBack)
    var resultNewCourses : List<NewCourse>
        get() = differ.currentList
        set(value) {differ.submitList(value)}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewCoursesViewHolder {
        return NewCoursesViewHolder(
            NewCoursesItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ))
    }

    override fun onBindViewHolder(holder: NewCoursesViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = resultNewCourses.size
}