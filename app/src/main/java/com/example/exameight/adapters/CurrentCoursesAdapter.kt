package com.example.exameight.adapters

import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.exameight.R
import com.example.exameight.databinding.CurrentCoursesItemBinding
import com.example.exameight.databinding.NewCoursesItemBinding
import com.example.exameight.models.ActiveCourse
import com.example.exameight.models.NewCourse

class CurrentCoursesAdapter : RecyclerView.Adapter<CurrentCoursesAdapter.CurrentCoursesViewHolder>(){

    inner class CurrentCoursesViewHolder(val binding: CurrentCoursesItemBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun bind()
        {
            binding.apply {
                val activeCourse = resultActiveCourses[adapterPosition]

                tvCourseName.text = activeCourse.title
                tvCourseName.setTextColor(Color.parseColor("#"+activeCourse.main_color))
                root.setCardBackgroundColor(Color.parseColor("#"+activeCourse.main_color))
                root.background.alpha = activeCourse.background_color_percent.toInt()

                ivBackground.setColorFilter(Color.parseColor("#"+activeCourse.main_color), PorterDuff.Mode.SRC_IN);

                Glide.with(ivIcon.getContext())  // glide with this view
                    .load(activeCourse.image)
                    .placeholder(R.mipmap.ic_launcher) // default placeHolder image
                    .error(R.mipmap.ic_launcher)
                    .centerCrop()// use this image if faile
                    .into(ivIcon)

                tvStartsAt.text = "Booked for " + activeCourse.booking_time
                tvStartsAt.setTextColor(Color.parseColor("#"+activeCourse.main_color))

                ivOval.setColorFilter(Color.parseColor("#"+activeCourse.main_color), PorterDuff.Mode.SRC_IN);
                //ivOval.background.alpha = activeCourse.play_button_color_percent.toInt()
            }

        }



    }


    private val diffCallBack = object: DiffUtil.ItemCallback<ActiveCourse>()
    {
        override fun areItemsTheSame(oldItem: ActiveCourse, newItem: ActiveCourse): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ActiveCourse, newItem: ActiveCourse): Boolean {
            return oldItem == newItem
        }

    }

    private val differ = AsyncListDiffer(this,diffCallBack)
    var resultActiveCourses : List<ActiveCourse>
        get() = differ.currentList
        set(value) {differ.submitList(value)}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrentCoursesViewHolder {
        return CurrentCoursesViewHolder(
            CurrentCoursesItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ))
    }

    override fun onBindViewHolder(holder: CurrentCoursesViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = resultActiveCourses.size
}